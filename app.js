
//--Every Look L'Oreal Proof Of Concept
//--alexsamuel@google.com
//------------------------



const request = require('request');
const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();

var http = require('http').Server(app);
var face = require('./app/faceDetection.js');

global.PORT = process.env.PORT || 8080;
global.log = console.log;
global._ = require('underscore');

app.use(express.static(__dirname + '/public'));
app.use(fileUpload());

app.get('/', function(req, res){
  res.sendfile('index.html');
});

app.post('/upload', function(req, res){
	
	if (!req.files)
    return res.status(400).send('No files were uploaded.');
 
  let sampleFile = req.files.img;
  let extension = sampleFile.name.substring(sampleFile.name.indexOf(".") + 1);
	let thisFilePath = './img/' + Date.now() + '.' + extension;

	sampleFile.mv(thisFilePath, function(err){
		if (err)
				return res.status(500).send(err);
		face.detect(thisFilePath, res); 

	});


});

http.listen(PORT, function(){
  log('Now listening on ' + PORT);


});





