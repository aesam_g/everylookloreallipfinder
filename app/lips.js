
//------------------------
//--Every Look L'Oreal Proof Of Concept
//--alexsamuel@google.com
//------------------------

/*TO DO: ERROR CATCH*/


const Jimp = require("jimp");
const ColorThief = require('color-thief')
const async = require('async');
const rgbHex = require('rgb-hex');
const fs = require('fs');

const colorMatch = require('./colorMatchRGB.js');

function lipColorFinder(img, boundaries, res){
	var img;
  var newImg; 
  var lipColor;
	var lipColorFinderRunOfShow = [];

	var UPPER_LIP_Y;
	var LOWER_LIP_Y;
	var MOUTH_LEFT_X;
	var MOUTH_RIGHT_X;
	var LIP_WIDTH;
	var LIP_HEIGHT;

	function pullLipCoordinates(cb){

		UPPER_LIP_Y = Math.round(boundaries.UPPER_LIP.y) + 5;
		LOWER_LIP_Y = Math.round(boundaries.LOWER_LIP.y) - 5;
		MOUTH_LEFT_X = Math.round(boundaries.MOUTH_LEFT.x) + 5;
		MOUTH_RIGHT_X = Math.round(boundaries.MOUTH_RIGHT.x) - 5;
		
		cb();
	};
	function calculateLipCoordinates(cb){

		LIP_WIDTH = MOUTH_RIGHT_X - MOUTH_LEFT_X;
		LIP_HEIGHT = LOWER_LIP_Y - UPPER_LIP_Y;

		cb();
	};
	function cropLips(cb){
		Jimp.read(img, function (err, image) {
	    image.crop(MOUTH_LEFT_X, UPPER_LIP_Y, LIP_WIDTH, LIP_HEIGHT); 
			image.write(newImg, cb ); 
		}).catch(function (err) {
	    console.log(err);
		});
	};
	function pullLipColor(cb){
		var colorThief = new ColorThief();
		lipColor = colorThief.getColor(newImg);
		//lipColor = '#' + rgbHex(lipColor[0], lipColor[1], lipColor[2]);
		lipColor = {'R': lipColor[0], 'G': lipColor[1], 'B': lipColor[2]};
		cb();
	};

	lipColorFinderRunOfShow.push(pullLipCoordinates);
	lipColorFinderRunOfShow.push(calculateLipCoordinates);
	lipColorFinderRunOfShow.push(cropLips);
	lipColorFinderRunOfShow.push(pullLipColor);

  newImg = './img-out/lips-' + Date.now() + '.png';

  async.series(lipColorFinderRunOfShow, function(){
		fs.unlinkSync(img);
		fs.unlinkSync(newImg);
  	colorMatch.match(lipColor, res);
  });

};

exports.getColor = lipColorFinder;