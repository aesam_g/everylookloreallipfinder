//TO DO: Color Match and response

const colors = require('./colors.js');
const cd = require('color-difference');
const async = require('async');



function matchLipstick(color, res){
	var colorFinderRunOfShow = [];
	var bestMatch = {};
	var currentCosmetic = colors.colourRiche;
	var allColorComps = [];
	var color = color;

	function compareAllColors(callback){

		async.each(currentCosmetic, function(thisCosmetic, cb){
			if(thisCosmetic.hex){
				thisCosmetic.score = cd.compare(thisCosmetic.hex, color);
				allColorComps.push(thisCosmetic);
				cb();}
			else{
				cb();}
		}, function(){
			callback();
		})

	};

	function sortColors(callback){
		allColorComps = _.sortBy(allColorComps, function(color){ return -color.score; });
		bestMatch = allColorComps[0];
		callback();

	}

	colorFinderRunOfShow.push(compareAllColors);
	colorFinderRunOfShow.push(sortColors);

	async.series(colorFinderRunOfShow, function(){
  	log(allColorComps[0]);
  	log(allColorComps[1]);
  	log(allColorComps[2]);
  	//TO DO: Why colors always the same?
  	//res.send(bestMatch);
  });

};



exports.match = matchLipstick;