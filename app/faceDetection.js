/**
 * Copyright 2016, Google, Inc.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//------------------------
//--ADAPTED FOR Every Look L'Oreal Proof Of Concept
//--alexsamuel@google.com
//------------------------

/*TO DO: ERROR CATCH*/

'use strict';

var Vision = require('@google-cloud/vision');
var vision = Vision();

var fs = require('fs');
var lips = require('./lips.js');


function findLipXY(inputFile, callback) {
  const request = { source: { filename: inputFile } };
  vision.faceDetection(request)
    .then((results) => {
      const faces = results[0].faceAnnotations;

      var lipBoundaries = {};
      _.each(faces, function(face){
        _.each(face.landmarks, function(landmark){
          if(landmark.type == 'UPPER_LIP')
            lipBoundaries.UPPER_LIP = landmark.position;
          if(landmark.type == 'LOWER_LIP')
            lipBoundaries.LOWER_LIP = landmark.position;
          if(landmark.type == 'MOUTH_LEFT')
            lipBoundaries.MOUTH_LEFT = landmark.position;
          if(landmark.type == 'MOUTH_RIGHT')
            lipBoundaries.MOUTH_RIGHT = landmark.position
        });
      });
      callback(null, lipBoundaries);
    })
    .catch((err) => {
      console.error('ERROR:', err);
      callback(err);
    });
}


function main (inputFile, Canvas, res, callback) {
  findLipXY(inputFile, (err, lipBoundaries) => {
    if (err) {
      return callback(err);
    }
    callback(inputFile, lipBoundaries, res);
  });
}

exports.main = main;
exports.detect = function(img, res){
  var inputFile = img;

  exports.main(inputFile, require('canvas'), res, lips.getColor);
};
