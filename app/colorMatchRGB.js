//TO DO: Color Match and response

const colors = require('./colorsRGB.js');
const cd = require('color-difference');
const async = require('async');
const diff = require('color-diff');
const log = console.log
const _ = require('underscore');

function matchLipstick(color, res){
	var colorFinderRunOfShow = [];
	
	var currentCosmetics = colors.colourRiche;
	var palette = _.pluck(colors.colourRiche, 'rgb');
	var rgbMatch = diff.closest(color, palette); 

	var productMatch = currentCosmetics.find(
		function findCosmetic(cosmetic) { 
	    return cosmetic.rgb === rgbMatch;
		});
	
	var name = productMatch.name.replace(/ /g, '-');
	productMatch.purchaseUrl = "https://www.lorealparisusa.com/products/makeup/lip-color/lipstick/colour-riche-lipcolour.aspx?&shade=" + name;
	res.send(productMatch);

};

exports.match = matchLipstick;