
//------------------------
//--NodeJS Template - JS
//--alexsamuel@google.com
//------------------------





function showLipstick(imgURL, lipstick){
  var source = $('#result_template').html();
  var template = Handlebars.compile(source);
  lipstick.referenceImage = imgURL;
  var result = template(lipstick);
  $('.input').hide();
  $('.output').append(result).fadeIn(500);
};


$(document).on('ready', function(){
  console.log("Ready!");


  Dropzone.options.upload = {
    paramName: "img", // The name that will be used to transfer the file
    maxFilesize: 1, // MB
    dictDefaultMessage: 'Drop your favorite lip pic here to find a matching lipstick.',
    success: function(file, serverResp){
      showLipstick(file.dataURL, serverResp);
    }
  };

  //TO DO: Add Error Reporting
  //TO DO: Add format check


});