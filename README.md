After installing this package, be sure to:

### Install Canvas
1. Install Canvas' dependencies
<br>
<code>
sudo apt-get install libcairo2-dev libjpeg8-dev libpango1.0-dev libgif-dev build-essential g++
</code>
<br>
2. Install Canvas
<br><code>npm install --save canvas</code>


### Install gCloud and Enable Cloud Vision API
1. Enable the Cloud Vision API from the cloud console.
2. Install gCloud/vision<br><code>npm install --save @google-cloud/vision</code>
3. Authenticate the instance <br><code>gcloud auth application-default login</code>
